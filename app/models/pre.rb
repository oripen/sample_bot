require "nokogiri"
require "open-uri"
require 'sanitize' 
  
  # input: HTML String
  # output: HTML String の二次元配列


  def copy_rowspan_content
      $html_in_row << $rowspan_count[$html_in_row.length]["content"]
      $rowspan_count[$html_in_row.length - 1]["remaining_rowspan"] -= 1
      if $rowspan_count[$html_in_row.length - 1]["remaining_rowspan"] == 0
        $rowspan_count.delete($html_in_row.length - 1)
      end
  end
  
  def record(rowspan_value)
    $rowspan_count[$html_in_row.length] = {"remaining_rowspan" => rowspan_value - 1, "content" => $cell_node.inner_html}
  end
  
  def record_rowspan(rowspan_value, colspan)
    if rowspan_value > 1
      record(rowspan_value)
      unless colspan.nil?
        (colspan.value.to_i - 1).times do
          $html_in_row << $cell_node.inner_html
          record(rowspan_value)
        end
      end
    end
  end
  
  def row_to_array(row)
    node_index = 0
    cell_nodeset = row.css("th,td")
    $html_in_row = []
    until (cell_nodeset.nil? or node_index >= cell_nodeset.length) and $rowspan_count[$html_in_row.length].nil?
      unless $rowspan_count[$html_in_row.length].nil?
        copy_rowspan_content
      else
        $cell_node = cell_nodeset[node_index]
        rowspan = $cell_node.attribute("rowspan")
        colspan = $cell_node.attribute("colspan")
        if not rowspan.nil?
          record_rowspan(rowspan.value.to_i, colspan)
        elsif not colspan.nil?
          (colspan.value.to_i - 1).times do
            $html_in_row << $cell_node.inner_html
          end
        end
        $html_in_row << $cell_node.inner_html
        node_index += 1
      end
    end 
    return $html_in_row
  end
  
  def table_to_array(table)
    begin
      doc = Nokogiri::HTML.parse(table)
    rescue => e
      puts "parse error"
    end
    #puts doc
    $rowspan_count = {}
    html_in_table = []
    row_nodeset = doc.css(".tableSheet tr")
    row_nodeset.each do |row|
      html_in_table << row_to_array(row)
    end
    return html_in_table
  end


=begin  
  # scraping #####
    html_konami = open('http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=006023')
    table = table_to_array(html_konami)
    
    bpArray = Array.new
    
    puts table[0][0]
    
    table.each{|row|
      dayJudge = row.length / 7
      for i in 0..row.length-1 do
        if row[i].include?("ボディパンプ")
          day = "7日"
          day = "6土" if i <= dayJudge * 6
          day = "5金" if i <= dayJudge * 5
          day = "4木" if i <= dayJudge * 4
          day = "3水" if i <= dayJudge * 3
          day = "2火" if i <= dayJudge * 2
          day = "1月" if i <= dayJudge * 1
          bpArray << day+Sanitize.clean(row[i])
        end
      end
    }
    
    bpArray.uniq!.sort!
    bpArray.each{|cell|
      cell.slice!(0)
    }
    puts bpArray.join("\n")
  ################
    
  # analisysMessage #####
    massage = ARGV[0]
    
    url_meguroaobadai = 'http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=004446'
    url_musashikosugi = 'http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=004070'
    url_shibuya       = 'http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=007871'
    url_meguro        = 'http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=006022'
    url_ebise         = 'http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=006023'
    
    text_meguroaobadai = '目黒青葉台'
    text_musashikosugi = '武蔵小杉'
    text_ebisu = '恵比寿'
    
    if massage != text_meguroaobadai && massage != text_musashikosugi && massage != text_ebisu 
     targetUrl = '未対応だよ'
    end
     
    targetUrl = url_meguroaobadai if massage == '目黒青葉台'
    targetUrl = url_musashikosugi if massage == '武蔵小杉'
    targetUrl = url_shibuya if massage == '渋谷'
    targetUrl = url_meguro if massage == '目黒'
    targetUrl = url_ebise if massage == '恵比寿'

    puts targetUrl
  ##########  
=end
  
  # forHoliday #####
    #'http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=004070'
    targetUrl = 'http://information.konamisportsclub.jp/newdesign/eventTimetable.php?facilityCd=004070'
    #targetUrl = 'http://information.konamisportsclub.jp/newdesign/eventTimetable.php?facilityCd=004446'
    
    html_konami1 = open(targetUrl)
    html_konami2 = open(targetUrl)
    
    doc = Nokogiri::HTML.parse(html_konami1)
    thArray = doc.css(".tableSheet th")
    puts thArray.length
    
    holidayName = Array.new
    thArray.each{|row|
      holidayName << Sanitize.clean(row)
    }
    
    table = table_to_array(html_konami2)
    
    holidayCount = thArray.length
    dayJudge = table[0].length / holidayCount
    
    bpArray = Array.new
    table.each{|row|
      for rowNo in 0..row.length-1 do
        if row[rowNo].include?("ボディパンプ")
          for dayNo in 0..holidayCount-1 do
            if rowNo <= dayJudge * dayNo+1
              day = dayNo.to_s+holidayName[dayNo]
              break
            end
          end
          bpArray << day+Sanitize.clean(row[rowNo])
        end
      end
    }
    puts bpArray.empty?
    if bpArray.empty?
      holidayName.each{|dayName|
        bpArray << dayName+"にボディパンプないよ"
      }
    end
    puts bpArray
    bpArray.uniq!.sort!
    bpArray.each{|cell|
      cell.slice!(0)
    }
    
    
    
    puts bpArray.join("\n")
  #########