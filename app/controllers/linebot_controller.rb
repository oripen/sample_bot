require "open-uri"
require "nokogiri"
require "sanitize"
require "read_schedule.rb"

class LinebotController < ApplicationController
  require 'line/bot'  # gem 'line-bot-api'

  # callbackアクションのCSRFトークン認証を無効
  protect_from_forgery :except => [:callback]

  def client
    @client ||= Line::Bot::Client.new { |config|
      config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
      config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
    }
  end

  def callback
    body = request.body.read
    
    signature = request.env['HTTP_X_LINE_SIGNATURE']
    unless client.validate_signature(body, signature)
      error 400 do 'Bad Request' end
    end

    events = client.parse_events_from(body)

    events.each { |event|
      case event
      when Line::Bot::Event::Message
        case event.type
        when Line::Bot::Event::MessageType::Text
          message = {
            type: 'text',
            text: event.message['text'] # analysisPostMassage(event.message['text'])
          }
          client.reply_message(event['replyToken'], message)
        end
      end
    }
    head :ok
  end
  
  
  def analysisPostMassage(massage)
    url_meguroaobadai = 'http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=004446'
    url_musashikosugi = 'http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=004070'
    url_ebise         = 'http://information.konamisportsclub.jp/newdesign/timetable.php?Facility_cd=006023'
    url_meguroaobadai_holiday = 'http://information.konamisportsclub.jp/newdesign/eventTimetable.php?facilityCd=004446'
    url_musashikosugi_holiday = 'http://information.konamisportsclub.jp/newdesign/eventTimetable.php?facilityCd=004070'
    url_ebise_holiday         = 'http://information.konamisportsclub.jp/newdesign/eventTimetable.php?facilityCd=006023'
    
    url_default = 'defo'
    
    text_meguroaobadai = '目黒青葉台'
    text_musashikosugi = '武蔵小杉'
    text_ebisu = '恵比寿'
    text_meguroaobadai_kyu = '目黒青葉台休日'
    text_musashikosugi_kyu = '武蔵小杉休日'
    text_ebisu_kyu = '恵比寿休日'
    text_meguroaobadai_syu = '目黒青葉台祝日'
    text_musashikosugi_syu = '武蔵小杉祝日'
    text_ebisu_syu = '恵比寿祝日'
    
    text_cannot = "未対応だよ"
    
    targetUrl = url_default
    targetUrl = url_meguroaobadai if massage == text_meguroaobadai
    targetUrl = url_musashikosugi if massage == text_musashikosugi
    targetUrl = url_ebise if massage == text_ebisu
    targetUrl = url_meguroaobadai_holiday if massage == text_meguroaobadai_kyu || massage == text_meguroaobadai_syu
    targetUrl = url_musashikosugi_holiday if massage == text_musashikosugi_kyu || massage == text_musashikosugi_syu
    targetUrl = url_ebise_holiday if massage == text_ebisu_kyu || massage == text_ebisu_syu
    
    if targetUrl = url_default 
      return text_cannot
    end
    
    if targetUrl == url_meguroaobadai || argetUrl == url_musashikosugi || targetUrl == url_ebise
      return ScrapingSchedule.new.scraipngKonami(targetUrl)
    end
    
    return ScrapingSchedule.new.scraipngKonamiForHoliday(targetUrl)
    
  end

end